import React from 'react';
import './Button.css';

const Button = props => {

  return (
    <button className="btn" onClick={props.generate}>Generate new field</button>
  );
};

export default Button;