import React from 'react';
import './Counter.css';

const Counter = props => {
  let message = `Total tries: ${props.counter}`;

  if (props.isFind) message = `Ring is finded for ${props.counter} tries`;

  return (
    <div>
      <span className="counter">{message}</span>
    </div>
  );
};

export default Counter;