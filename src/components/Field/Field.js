import React from 'react';
import Plate from "../Plate/Plate";

const Field = props => {
  return props.plates.map((plate) => {
    let classes = 'plate';
    classes += plate.hasItem ? ' with-item' : '';
    classes += plate.isClicked ? ' opened' : '';

    let click = (event) => props.clicked(event, plate.id);
    if (props.isFind) click = () => {};

    return (
      <Plate
        class={classes}
        clicked={click}
        key={plate.id}
      />
    );
  })
};

export default Field;