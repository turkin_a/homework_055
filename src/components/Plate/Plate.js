import React from 'react';
import './Plate.css';

const Plate = props => {
  return (
    <div
      className={props.class}
      onClick={props.clicked}
    />
  );
};

export default Plate;