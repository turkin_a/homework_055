import React, { Component } from 'react';
import './App.css';
import Field from "../components/Field/Field";
import Button from "../components/Button/Button";
import Counter from "../components/Counter/Counter";

class App extends Component {
  state = {
    plates: [],
    counter: 0,
    isFind: false
  };

  generatePlates = () => {
    let plates = [];
    let counter = 0;
    let isFind = false;

    for (let i = 0; i < 36; i++) {
      let plate = {
        hasItem: false,
        isClicked: false,
        id: i
      };

      plates.push(plate);
    }

    plates[Math.floor(Math.random() * 36)].hasItem = true;

    console.log(plates);

    this.setState({plates, counter, isFind});
  };

  clicked = (event, id) => {
    const index = this.state.plates.findIndex(p => p.id === id);
    let plates = [...this.state.plates];
    let counter = this.state.counter;
    let isFind = this.state.isFind;

    if (!plates[index].isClicked) counter++;
    plates[index].isClicked = true;

    if (plates[index].hasItem) isFind = true;

    this.setState({plates, counter, isFind});
  };

  render() {
    return (
      <div className="App">
        <div className="field">
          <Field
            plates={this.state.plates}
            clicked={this.clicked}
            isFind={this.state.isFind}/>
        </div>
        <Counter counter={this.state.counter} isFind={this.state.isFind}/>
        <Button generate={this.generatePlates}/>
      </div>
    );
  }
}

export default App;
